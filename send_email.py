import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

# Email configuration
SMTP_SERVER = "smtp.gmail.com"
SMTP_PORT = 587
EMAIL_FROM = "cloudengineer1987@gmail.com"
EMAIL_PASSWORD = "xwtr disx jpni xzna"  # Generate App Password: https://support.google.com/accounts/answer/185833
EMAIL_TO = ["praveenreddy15@gmail.com"]
EMAIL_SUBJECT = "CI/CD Artifacts from GitLab"

def send_email():
    msg = MIMEMultipart()
    msg["From"] = EMAIL_FROM
    msg["To"] = ", ".join(EMAIL_TO)
    msg["Subject"] = EMAIL_SUBJECT

    # Attach report file
    attachment = MIMEApplication(open("reports/report.html", "rb").read())
    attachment.add_header("Content-Disposition", "attachment", filename="report.html")
    msg.attach(attachment)

    # Connect to SMTP server and send email
    with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as server:
        server.starttls()
        server.login(EMAIL_FROM, EMAIL_PASSWORD)
        server.send_message(msg)

if __name__ == "__main__":
    send_email()
